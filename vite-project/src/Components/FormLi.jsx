import React, {useEffect,useState,useRef} from 'react'; 
import {sendRequest} from '../functions';
import DivInput from './DivInput';
import DivSelect from './DivSelect';
const FormLi = (params) => {
    const [titulo,setTitulo] = useState('');
    const [genero,setGenero] = useState('');
    const [año,setAño]=useState('');
    const [cantidad,setCantidad]=useState('');
    const [autores,setAutores]=useState([]);
    const [autorId,setAutorId]=useState('');
    const TituloInput = useRef();
    let method = 'POST';
    let url = '/api/libros';
    let redirect ='';
    useEffect(() =>{
        TituloInput.current.focus();
        getLibros();
        getAutores();
    },[]);
    const getLibros = async() =>{
        if(params.id !== null){
            const res = await sendRequest('GET','',(url+'/'+params.id)); 
            setTitulo(res.data.titulo);
            setGenero(res.data.genero);
            setAño(res.data.ano_publicacion);
            setCantidad(res.data.cantidad_disponible);
            setAutorId(res.data.autor_id);

        }
    }
    const getAutores = async() =>{
        const res = await sendRequest('GET','','/api/autores'); 
        setAutores(res);
        console.log(res);
}
    const save = async(e) =>{
    e.preventDefault();
    if(params.id !== null){
        method= 'PUT';
        url = '/api/libros/'+params.id;
        redirect = '/';
    }
    const res = await sendRequest(method, {titulo:titulo,genero:genero,ano_publicacion:año,
    cantidad_disponible:cantidad,autor_id:autorId}, url, redirect); 
    if(method == 'POST' && res.status == true){ 
            setTitulo('');
            setGenero('');
            setAño('');
            setCantidad('');
            setAutorId('');
    }
}

    return (
        <div className='container-fluid'>
            <div className='row mt-5'>
                <div className='col-md-4 offset-md-4'>
                    <div className='card border border-info'>
                        <div className='card-header bg-info border border-info'>
                           {params.title}
                        </div>
                        <div className='card-body'>
                            <form onSubmit={save}>
                                <DivInput type='text' icon='fa-building' 
                                value={titulo} className='form-control' 
                                placeholder='Título' required='required' 
                                ref={TituloInput} 
                                handleChange={(e)=>setTitulo(e.target.value)} />
                                <DivInput type='text' icon='fa-building' 
                                value={genero} className='form-control' 
                                placeholder='Género' required='required' 
                                handleChange={(e)=>setGenero(e.target.value)} />
                                <DivInput type='date' icon='fa-building' 
                                value={año} className='form-control' 
                                placeholder='Name' required='required' 
                                handleChange={(e)=>setAño(e.target.value)} />
                                <DivInput type='text' icon='fa-building' 
                                value={cantidad} className='form-control' 
                                placeholder='Cantidad' required='required' 
                                handleChange={(e)=>setCantidad(e.target.value)} />

                                <div className='input-group mb-3'>
                                  <span className='input-group-text'>
                                <i className={'fa-solid fa-user'}></i> 
                                  </span>
                                 <select className='form-select' value={autorId}
                                 required='required' onChange={(e) => setAutorId(e.target.value)} > 
                                 <option defaultValue={true}> 
                                 </option> 
                                     {autores.map( (op) => (
                                    <option value={op.id} key={op.id}>{op.nombre}</option>
                                   )) }
                                     </select>

                             </div>                            
                                <div className='d-grid col-10 mx-auto'> 
                                    <button className='btn btn-dark'>
                                        <i className='fa-solid fa-save'></i>
                                    Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )    
}
export default FormLi