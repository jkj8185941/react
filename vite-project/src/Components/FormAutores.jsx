import React, {useEffect,useState,useRef} from 'react'; 
import {sendRequest} from '../functions';
import DivInput from './DivInput';
import DivSelect from './DivSelect';
const FormAutores = (params) => {
    const [nombre,setNombre] = useState('');
    const [nacionalidad,setNacionalidad] = useState('');
    const [fecha,setFecha]=useState('');

    let method = 'POST';
    let url = '/api/autores';
    let redirect ='';
    useEffect(() =>{
        TituloInput.current.focus();
        getAutores();
    },[]);
    const getAutores = async() =>{
        if(params.id !== null){
            const res = await sendRequest('GET','','/api/autores'); 
            setAutores(res);
            console.log(res);

        }
    }
    const save = async(e) =>{
    e.preventDefault();
    if(params.id !== null){
        method= 'PUT';
        url = '/api/autores/'+params.id;
        redirect = '/';
    }
    const res = await sendRequest(method, {nombre:nombre,nacionalidad:nacionalidad,fecha_nacimiento:fecha}, url, redirect); 
    if(method == 'POST' && res.status == true){ 
        setNombre('');
        setNacionalidad('');
        setFecha('');
    }
}

    return (
        <div className='container-fluid'>
            <div className='row mt-5'>
                <div className='col-md-4 offset-md-4'>
                    <div className='card border border-info'>
                        <div className='card-header bg-info border border-info'>
                           {params.title}
                        </div>
                        <div className='card-body'>
                            <form onSubmit={save}>
                                <DivInput type='text' icon='fa-building' 
                                value={nombre} className='form-control' 
                                placeholder='Nombre' required='required' 
                                ref={TituloInput} 
                                handleChange={(e)=>setNombre(e.target.value)} />
                                <DivInput type='text' icon='fa-building' 
                                value={nacionalidad} className='form-control' 
                                placeholder='Nacionalidad' required='required' 
                                handleChange={(e)=>setNacionalidad(e.target.value)} />
                                <DivInput type='date' icon='fa-building' 
                                value={fecha} className='form-control' 
                                placeholder='Fecha' required='required' 
                                handleChange={(e)=>setFecha(e.target.value)} />
                                <div className='d-grid col-10 mx-auto'> 
                                    <button className='btn btn-dark'>
                                        <i className='fa-solid fa-save'></i>
                                    Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )    
}
export default FormAutores