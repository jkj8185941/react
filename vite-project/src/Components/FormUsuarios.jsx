import React, {useEffect,useState,useRef} from 'react'; 
import {sendRequest} from '../functions';
import DivInput from './DivInput';
import DivSelect from './DivSelect';
const FormUsuarios = (params) => {
    const [nombre,setNombre] = useState('');
    const [correo,setCorreo] = useState('');
    const [telefono,setTelefono]=useState('');
    const [direccion,setDireccion]=useState('');
    const [fechar,setFechar]=useState('');
    const TituloInput = useRef();
    let method = 'POST';
    let url = '/api/usuarios';
    let redirect ='';
    useEffect(() =>{
        TituloInput.current.focus();
        getUsuarios();
        getAutores();
    },[]);
    const getUsuarios = async() =>{
        if(params.id !== null){
            const res = await sendRequest('GET','',(url+'/'+params.id)); 
            setNombre(res.data.nombre);
            setCorreo(res.data.correo);
            setTelefono(res.data.telefono);
            setDireccion(res.data.direccion);
            setFechar(res.data.fecha_registro);

        }
    }
    const save = async(e) =>{
    e.preventDefault();
    if(params.id !== null){
        method= 'PUT';
        url = '/api/usuarios/'+params.id;
        redirect = '/';
    }
    const res = await sendRequest(method, {nombre:nombre,correo:correo,telefono:telefono,direccion:direccion,fecha_registro:fechar}, url, redirect); 
    if(method == 'POST' && res.status == true){ 
            setTitulo('');
            setGenero('');
            setAño('');
            setCantidad('');
            setAutorId('');
    }
}

    return (
        <div className='container-fluid'>
            <div className='row mt-5'>
                <div className='col-md-4 offset-md-4'>
                    <div className='card border border-info'>
                        <div className='card-header bg-info border border-info'>
                           {params.title}
                        </div>
                        <div className='card-body'>
                            <form onSubmit={save}>
                                <DivInput type='text' icon='fa-building' 
                                value={nombre} className='form-control' 
                                placeholder='Nombre' required='required' 
                                ref={TituloInput} 
                                handleChange={(e)=>setNombre(e.target.value)} />
                                <DivInput type='text' icon='fa-building' 
                                value={correo} className='form-control' 
                                placeholder='Correo' required='required' 
                                handleChange={(e)=>setCorreo(e.target.value)} />
                                <DivInput type='text' icon='fa-building' 
                                value={telefono} className='form-control' 
                                placeholder='Telefono' required='required' 
                                handleChange={(e)=>setTelefono(e.target.value)} />
                                <DivInput type='text' icon='fa-building' 
                                value={direccion} className='form-control' 
                                placeholder='Direccion' required='required' 
                                handleChange={(e)=>setDireccion(e.target.value)} />

<DivInput type='text' icon='fa-building' 
                                value={fechar} className='form-control' 
                                placeholder='Direccion' required='required' 
                                handleChange={(e)=>setFechar(e.target.value)} />                      
                                <div className='d-grid col-10 mx-auto'> 
                                    <button className='btn btn-dark'>
                                        <i className='fa-solid fa-save'></i>
                                    Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )    
}
export default FormUsuarios