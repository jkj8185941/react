import { BrowserRouter, Routes, Route } from "react-router-dom"; 
import Nav from './Components/Nav';
import Libros from './Views/Libros/Index';
import CreateLibros from './Views/Libros/Create';
import EditLibros from './Views/Libros/Edit';
import GraphicAutores from './views/Autores/Graphic';
import Login from './Views/Login';
import Register from './Views/Register';
import Prestamos from './views/Prestamos/Index';
import Usuarios from './Views/Usuarios/Index';
import ProtectedRoutes from './Components/ProtectedRoutes';

import Autores from "./Views/Autores/Index";
import EditAutores from "./Views/Autores/Edit";

function App() {
  
  return (

  <BrowserRouter>
  <Nav />
  <Routes>
<Route path="/login" element={<Login />} />
<Route path="/register" element={<Register />} /> 
<Route element={<ProtectedRoutes />}>
<Route path="/" element={<Libros />} /> 
<Route path="/create" element={<CreateLibros />} /> 
<Route path="/editlibros/:id" element={<EditLibros />} /> 

<Route path="/autores" element={<Autores />} />
<Route path="/createautores" element={<CreateLibros />} /> 
<Route path="/editautores/:id" element={<EditLibros />} /> 

<Route path="/" element={<Libros />} /> 
<Route path="/create" element={<CreateLibros />} /> 
<Route path="/edit/:id" element={<EditLibros />} /> 


<Route path="/" element={<Libros />} /> 
<Route path="/create" element={<CreateLibros />} /> 
<Route path="/edit/:id" element={<EditLibros />} /> 

<Route path="/graphic" element={<GraphicAutores />} /> 
<Route path="/prestamos" element={<Prestamos />} /> 
<Route path="/usuarios" element={<Usuarios />} /> 
</Route>
</Routes>
  </BrowserRouter>
  )
 }

 export default App
