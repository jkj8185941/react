import React, {useEffect, useState} from 'react';
import DivAdd from '../../Components/DivAdd';
import DivTable from '../../Components/DivTable';
import { Link } from 'react-router-dom';
import {confirmation, sendRequest} from '../../functions';

const Usuarios = () => {
  const [usuarios, setUsuarios] = useState([]); 
    const [classLoad, setClassLoad] = useState(''); 
    const [classTable,setClassTable] = useState('d-none'); 
    useEffect(() =>{
        getUsuarios();
    },[]);
    const getUsuarios = async() =>{
        const res = await sendRequest('GET','','/api/usuarios','');
        setUsuarios (res);
        setClassTable('');
        setClassLoad('d-none');
    }
        const deleteUsuario = (id,name) =>{
            confirmation(name,'/api/usuarios/'+id,'/');
        }
return (
<div className='container-fluid'>
                <DivAdd>
                    <Link to='/create' className='btn btn-dark'>
                        <i className='fa-solid fa-circle-plus'></i> Add
                    </Link>
                </DivAdd>
                <DivTable col='6' off='3' classLoad={classLoad} classTable={classTable}> 
                <table className='table table-bordered'>
                    <thead><tr><th>#</th><th>Usuario</th><th>Correo</th><th>Teléfono</th><th>Dirección</th><th>Fecha de registro</th></tr></thead>
                    <tbody className='table-group-divider'> 
                        {usuarios.map( (row,i)=>(
                            <tr key={row.id}> 
                                <td>{(i+1)}</td>
                                <td>{row.nombre}</td>
                                <td>{row.correo}</td>
                                <td>{row.telefono}</td>
                                <td>{row.direccion}</td>
                                <td>{row.fecha_registro}</td>
                                <td>
                                    <Link to={'/edit/'+row.id} className='btn btn-warning'>
                                        <i className='fa-solid fa-edit'></i>
                                    </Link>
                                </td>
                                <td>
                                    <button className='btn btn-danger'
                                    onClick={() => deleteUsuario(row.id,row.nombre)}>
                                        <i className='fa-solid fa-trash'></i>
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                </DivTable>
            </div>
)
}
export default Usuarios