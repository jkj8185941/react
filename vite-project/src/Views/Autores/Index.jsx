import React, {useEffect, useState} from 'react';
import DivAdd from '../../Components/DivAdd';
import DivTable from '../../Components/DivTable';
import { Link } from 'react-router-dom';
import {confirmation, sendRequest} from '../../functions';

const Autores = () => {
  const [autores, setAutores] = useState([]); 
    const [classLoad, setClassLoad] = useState(''); 
    const [classTable,setClassTable] = useState('d-none'); 
    useEffect(() =>{
        getAutores();
    },[]);
    const getAutores = async() =>{
        const res = await sendRequest('GET','','/api/autores','');
        setAutores (res);
        setClassTable('');
        setClassLoad('d-none');
    }
        const deleteAutor = (id,name) =>{
            confirmation(name,'/api/autores/'+id,'/');
        }
  
return (
<div className='container-fluid'>
                <DivAdd>
                    <Link to='/create' className='btn btn-dark'>
                        <i className='fa-solid fa-circle-plus'></i> Add
                    </Link>
                </DivAdd>
                <DivTable col='6' off='3' classLoad={classLoad} classTable={classTable}> 
                <table className='table table-bordered'>
                    <thead><tr><th>#</th><th>Autor</th><th>nacionalidad</th><th>Fecha de nacimiento</th></tr></thead>
                    <tbody className='table-group-divider'> 
                        {autores.map( (row,i)=>(
                            <tr key={row.id}> 
                                <td>{(i+1)}</td>
                                <td>{row.nombre}</td>
                                <td>{row.nacionalidad}</td>
                                <td>{row.fecha_nacimiento}</td>
                                <td>
                                    <Link to={'/edit/'+row.id} className='btn btn-warning'>
                                        <i className='fa-solid fa-edit'></i>
                                    </Link>
                                </td>
                                <td>
                                    <button className='btn btn-danger'
                                    onClick={() => deleteAutor(row.id,row.nombre)}>
                                        <i className='fa-solid fa-trash'></i>
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                </DivTable>
            </div>
            )

}

export default Autores