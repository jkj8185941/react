import React from 'react';
import { useParams } from 'react-router-dom'; 
import FormAutores from '../../Components/FormAutores';
const EditAutores = () => {
const {id} = useParams();
return (
<FormAutores id={id} title='Edit Libro'></FormAutores>
) 
}
export default EditAutores