import React from 'react';
import { useParams } from 'react-router-dom'; 
import FormLi from '../../Components/FormLi';
const EditLibros = () => {
const {id} = useParams();
return (
<FormLi id={id} title='Edit Libro'></FormLi>
) 
}
export default EditLibros