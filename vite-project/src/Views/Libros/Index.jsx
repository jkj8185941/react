import React, {useEffect, useState} from 'react';
import DivAdd from '../../Components/DivAdd';
import DivTable from '../../Components/DivTable';
import { Link } from 'react-router-dom';
import {confirmation, sendRequest} from '../../functions';
const Libros = () => {
    const [libros, setLibros] = useState([]); 
    const [classLoad, setClassLoad] = useState(''); 
    const [classTable,setClassTable] = useState('d-none'); 
    useEffect(() =>{
        getLibros();
    },[]);
    const getLibros = async() =>{
        const res = await sendRequest('GET','','/api/libros','');
        setLibros (res);
        setClassTable('');
        setClassLoad('d-none');
    }
        const deleteLibro = (id,name) =>{
            confirmation(name,'/api/libros/'+id,'/');
        }
        return (
            <div className='container-fluid'>
                <DivAdd>
                    <Link to='/create' className='btn btn-dark'>
                        <i className='fa-solid fa-circle-plus'></i> Add
                    </Link>
                </DivAdd>
                <DivTable col='6' off='3' classLoad={classLoad} classTable={classTable}> 
                <table className='table table-bordered'>
                    <thead><tr><th>#</th><th>LIBRO</th><th>Autor</th><th>Género</th><th>Año</th><th>Cantidad</th></tr></thead>
                    <tbody className='table-group-divider'> 
                        {libros.map( (row,i)=>(
                            <tr key={row.id}> 
                                <td>{(i+1)}</td>
                                <td>{row.titulo}</td>
                                <td>{row.autor_id}</td>
                                <td>{row.genero}</td>
                                <td>{row.ano_publicacion}</td>
                                <td>{row.cantidad_disponible}</td>
                                <td>
                                    <Link to={'/edit/'+row.id} className='btn btn-warning'>
                                        <i className='fa-solid fa-edit'></i>
                                    </Link>
                                </td>
                                <td>
                                    <button className='btn btn-danger'
                                    onClick={() => deleteLibro(row.id,row.titulo)}>
                                        <i className='fa-solid fa-trash'></i>
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                </DivTable>
            </div>
        )
}

export default Libros